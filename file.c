#include <stdio.h>
#include <stdlib.h>


typedef void(*multFunc)(struct MyStruct_Mat*, struct MyStruct_Mat*, struct MyStruct_Mat*);
typedef void(*addFunc)(struct MyStruct_Mat*, struct MyStruct_Mat*, struct MyStruct_Mat*);
typedef void(*vmethod)(void* matrix);
vmethod *vtable_vec_class;
vmethod *vtable_mat_class;

typedef struct MyStruct_Mat {
	int rows;
	int col;
	int *structArr;
	multFunc multiply;
	addFunc addition;
	vmethod *vtable_ptr;

} MyStruct_Mat;

typedef struct MyStruct_Vec
{	int min;
	int max;
	MyStruct_Mat mat;
	vmethod *vtableptr;
} MyStruct_Vec;

void mat_L1_Norm(MyStruct_Mat* this)
{
	int i = 0, j = 0;
	printf("\nSum of columns:\n");
	for (i = 0; i < this->rows; i++)
	{
		this->structArr[i] = 0;
		for (j = 0; j < this->col ; j++)
		{
			
			this->structArr[i] += this->structArr[i*this->col + j];
			
		}
		printf("%d\n", this->structArr[i]);
	}
	int max;
	max = this->structArr[0];
	for (i = 1; i < this->rows; i++)
	{
		if (this->structArr>max)
		{
			max = this->structArr[i];

		}
	}
	printf("\nL1 norm (max of the sums above): %d\n\n", max);
}
void vec_L1_Norm(MyStruct_Mat* this)
{
	
	struct MyStruct_Vec* v = (MyStruct_Vec*)this;
	
	int j= 0, sum=0;
	
		for (j = 0; j < v->mat.col; j++)
		{

			sum += v->mat.structArr[j];
			
		}
		printf("\nL1 norm (sum of all elements): %d\n\n", sum);
}
void matMult(MyStruct_Mat *m1, MyStruct_Mat *m2, MyStruct_Mat *mR)
{
	int i = 0, j = 0, k = 0;
	for (i = 0; i < m1->rows; i++)
	{
		for (j = 0; j < m2->col; j++)
		{
			int sum = 0;
			for (k = 0; k < m1->col; k++)
			{
				sum += m1->structArr[i*m1->col + k] * m2->structArr[k*m2->col + j];

			}
			mR->structArr[i*mR->col + j] = sum;
			printf("%d ", mR->structArr[i*mR->col + j]);
		}
		printf(" \n");
	}
}
void vecAddition(MyStruct_Vec *v1, MyStruct_Vec *v2, MyStruct_Vec *vR)
{
	int i = 0, j = 0;
	for (i = 0; i < 1; i++)
	{

		for (j = 0; j < v1->mat.col; j++)
		{
			vR->mat.structArr[i*vR->mat.col + j] = v1->mat.structArr[i*v1->mat.col + j] + v2->mat.structArr[i*v2->mat.col + j];
			printf("%d ", vR->mat.structArr[i*vR->mat.col + j]);
		}
		printf("\n");
	}
}


void matAdd(MyStruct_Mat *m1, MyStruct_Mat *m2, MyStruct_Mat *mR)
{
	int i = 0, j = 0;
	for (i = 0; i < m1->rows; i++)
	{
		for (j = 0; j < m1->col; j++)
		{
			mR->structArr[i*mR->col + j] = m1->structArr[i*m1->col+j] +  m2->structArr[i*m2->col + j];
			printf("%d ", mR->structArr[i*mR->col + j]);
		}
		printf("\n");
	}
}
void init_myStructMat(MyStruct_Mat *y, int r, int c)
{
	y->rows = r;
	y->col = c;
	y->addition = matAdd;
	y->multiply = matMult;
	y->structArr = malloc(sizeof(int)*r*c);
	int i = 0, j = 0;
	for (i = 0; i < r; i++)
	{
		for (j = 0; j < c; j++)
		{
			y->structArr[i*c + j] = j + i;
		}
	}
}

void init_myStructVec(MyStruct_Vec *y1, int c1)
{
	y1->mat.col = c1;
	y1->mat.rows = 1;
	y1->mat.addition=vecAddition;
	y1->min=0;
	y1->max=0;
	y1->mat.structArr = malloc(sizeof(int)*y1->mat.col);
	int i = 0, j = 0;
	for (i = 0; i < 1; i++)
	{
		for (j = 0; j < c1; j++)
		{
			y1->mat.structArr[i*c1 + j] = j + i;
		}
	}
}

void main()
{
	vtable_vec_class = (vmethod*)malloc(sizeof(vmethod));
	vtable_mat_class = (vmethod*)malloc(sizeof(vmethod));
	vtable_vec_class[0]=vec_L1_Norm;
	vtable_mat_class[0]=mat_L1_Norm;
	printf("				MATRIX COMPUTATIONS\n\n");
	MyStruct_Mat m1;
	MyStruct_Mat mRes;
	init_myStructMat(&m1, 3, 3);
	init_myStructMat(&mRes, 3, 3);
	int i = 0, j = 0;
	printf("Matrix: \n");
	for (i = 0; i <m1.rows; i++)
	{
		for (j = 0; j < m1.col; j++)
		{
			printf("%d ", m1.structArr[i * m1.col + j]);
		}
		printf("\n");
	}
	printf("\nNOTE:I'm adding and multiplying the matrix with itself.\n");
	printf("\n");
	printf("Addition resultant:\n");
	mRes.addition(&m1, &m1, &mRes);
	printf("\n");
	printf("Multiplication Resultant:\n");
	mRes.multiply(&m1, &m1, &mRes);
	mRes.vtable_ptr=vtable_mat_class;
	mRes.vtable_ptr[0](&m1);

	printf("				VECTOR COMPUTATIONS\n\n");
	MyStruct_Vec v1;
	MyStruct_Vec vRes;
	init_myStructVec(&v1, 5);
	init_myStructVec(&vRes, 5);
	printf("Vector: \n");
	for (i = 0; i <1; i++)
	{
		for (j = 0; j < v1.mat.col; j++)
		{
			printf("%d ", v1.mat.structArr[i * m1.col + j]);
		}
		printf("\n");
	}
	printf("\nNOTE:I'm adding the vector to itself.\n");
	printf("\n");
	printf("Addition resultant:\n");
	vRes.mat.addition(&v1, &v1, &vRes);
	vRes.vtableptr = vtable_vec_class;
	vRes.vtableptr[0](&v1);
	printf("\n");
	
	_getch();
}
